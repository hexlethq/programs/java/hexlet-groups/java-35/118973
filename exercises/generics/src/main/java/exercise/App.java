package exercise;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
//import java.util.Map.Entry;

// BEGIN
public class App {
    public static List findWhere(List<Map<String, String>> books, Map<String, String> searchingKeys) {
        List<Map> result = new ArrayList<>();

        for (Map<String, String> book: books) {
            if (book.entrySet().containsAll(searchingKeys.entrySet())) {
                result.add(book);
            }
        }
        return result;
    }
}
//END
