package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    void testTake() {
        // BEGIN
        List <Integer> numbers1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        List <Integer> actual1 = App.take(numbers1, 3);
        List <Integer> expected1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        assertThat(actual1).isEqualTo(expected1);

        List <Integer> numbers2 = new ArrayList<>();
        List <Integer> actual2= App.take(numbers2, 4);
        assertThat(actual2).isEmpty();

        List <Integer> numbers3 = new ArrayList<>(Arrays.asList(1, 2, 3));
        List <Integer> actual3 = App.take(numbers3, 8);
        List <Integer> expected3 = new ArrayList<>(Arrays.asList(1, 2, 3));
        assertThat(numbers3).isEqualTo(expected3);

        List <Integer> numbers4 = new ArrayList<>(Arrays.asList(7, 15, 23, 4));
        List <Integer> actual4 = App.take(numbers4, 0);
        assertThat(actual4).isEmpty();
        // END
    }
}
