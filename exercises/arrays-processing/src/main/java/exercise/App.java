package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int [] arr){
        int index = -1;
        int maxNegative = Integer.MIN_VALUE;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0 && arr[i] > maxNegative ) {
                maxNegative = arr[i];
                index = i;
            }
        }

        return index;
    }

    public static int[] getElementsLessAverage(int[] arr){
        if (arr.length == 0) {
            return (new int[0]);
        }

        double avgValue = 0.0;
        for (int x: arr) {
            avgValue += x;
        }
        avgValue /= arr.length;

        int resultArrLength = 0;
        for (int x : arr) {
            if ((double) x <= avgValue) {
                resultArrLength++;
            }
        }

        int[] resultArr = new int[resultArrLength];
        int currIndex = 0;
        for (int x : arr) {
            if ((double) x <= avgValue) {
                resultArr[currIndex] = x;
                currIndex++;
            }
        }

        return resultArr;
    }
    // END
}
