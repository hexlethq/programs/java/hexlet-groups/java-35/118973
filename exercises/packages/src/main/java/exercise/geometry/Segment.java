// BEGIN
package exercise.geometry;

public class Segment {
    public static double[][] makeSegment(double[] firstPoint, double[] secondPoint) {
        double [][] segment = {firstPoint, secondPoint};
        return segment;
    }

    public static double[] getBeginPoint(double[][] segment) {
        double[] beginPoint = segment[0];
        return beginPoint;
    }

    public static double[] getEndPoint(double[][] segment){
        double[] endPoint = segment[1];
        return endPoint;
    }
}
// END
