// BEGIN
package exercise;

import exercise.geometry.Point;
import static exercise.geometry.Segment.getBeginPoint;
import static exercise.geometry.Segment.getEndPoint;

class App {
    public static double[] getMidpointOfSegment(double [][] segment) {
        double midX = (Point.getX(getBeginPoint(segment)) + Point.getX(getEndPoint(segment))) / 2;
        double midY = (Point.getY(getBeginPoint(segment)) + Point.getY(getEndPoint(segment))) / 2;

        return new double[] {midX, midY};
    }

    public static double[][] reverse(double [][] segment) {
        return new double[][] {segment[1].clone(), segment[0].clone()};
    }

    public static boolean isBelongToOneQuadrant(double [][] segment) {
        if (getQuadrantOfPoint(getBeginPoint(segment)) == 0 || getQuadrantOfPoint(getEndPoint(segment)) == 0) {
            return false;
        }
        return (getQuadrantOfPoint(getBeginPoint(segment)) == getQuadrantOfPoint(getEndPoint(segment)));
    }

    public static int getQuadrantOfPoint(double[] point){
        if (Point.getX(point) > 0 && Point.getY(point) > 0) {
            return 1;
        }
        else if (Point.getX(point) < 0 && Point.getY(point) > 0) {
            return 2;
        }
        else if (Point.getX(point) < 0 && Point.getY(point) < 0) {
            return 3;
        }
        else if (Point.getX(point) > 0 && Point.getY(point) < 0) {
            return 4;
        }
        return 0;
    }
}
// END
