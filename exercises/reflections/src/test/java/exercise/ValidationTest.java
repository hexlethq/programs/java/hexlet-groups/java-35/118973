package exercise;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Map;


class ValidationTest {

    @Test
    void testValidate() {
        Address address1 = new Address("Russia", "Ufa", "Lenina", "54", null);
        List<String> result1 = Validator.validate(address1);
        List<String> expected1 = List.of();
        assertThat(result1).isEqualTo(expected1);

        Address address2 = new Address(null, "London", "1-st street", "5", "1");
        List<String> result2 = Validator.validate(address2);
        List<String> expected2 = List.of("country");
        assertThat(result2).isEqualTo(expected2);

        Address address3 = new Address("USA", null, null, null, "1");
        List<String> result3 = Validator.validate(address3);
        List<String> expected3 = List.of("city", "street", "houseNumber");
        assertThat(result3).isEqualTo(expected3);
    }

    // BEGIN
    @Test
    void testAdvancedValidate1() {
        Address address = new Address("USA", "Texas", null, "7", "2");
        Map<String, List<String>> notValidFields = Validator.advancedValidate(address);

        String actual = notValidFields.toString();
        String expected = "{country=[length less than 4], street=[can not be null]}";
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testAdvancedValidate2() {
        Address address = new Address("Russia", "Saint - Petersburg",
                "street", "7", "2");
        Map<String, List<String>> notValidFields = Validator.advancedValidate(address);

        String actual = notValidFields.toString();
        String expected = "{}";
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testAdvancedValidate3() {
        Address address = new Address(null, null, null, null, "2");
        Map<String, List<String>> notValidFields = Validator.advancedValidate(address);

        String actual = notValidFields.toString();
        String expected = "{country=[can not be null], city=[can not be null]," +
                " street=[can not be null], houseNumber=[can not be null]}";
        Assertions.assertEquals(expected, actual);
    }
    // END
}
