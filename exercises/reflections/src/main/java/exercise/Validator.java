package exercise;

import java.lang.reflect.Field;
import java.util.*;

// BEGIN
class Validator {

    public static final List<String> validate(Address address) {
        Field[] fields = address.getClass().getDeclaredFields();
        return Arrays.stream(fields)
                .filter(field -> field.getAnnotation(NotNull.class) != null)
                .filter(field -> isNullField(field, address))
                .map(field -> field.getName())
                .toList();
    }

    public static final List<String> validateForMinLength(Address address) {
        Field[] fields = address.getClass().getDeclaredFields();
        return Arrays.stream(fields)
                .filter(field -> field.getAnnotation(MinLength.class) != null)
                .filter(field -> isTooShortString(field, address))
                .map(field -> field.getName())
                .toList();
    }

    public static final Map<String, List<String>> advancedValidate(Address address) {
        Field[] fields = address.getClass().getDeclaredFields();
        Map<String, List<String>> incorrectlyFilledFields = new HashMap<>();

        List<String> nullFields = validate(address);
        List<String> tooShortFields = validateForMinLength(address);

        nullFields.forEach(field -> incorrectlyFilledFields.put(field, List.of("can not be null")));
        tooShortFields.forEach(field -> incorrectlyFilledFields.put(field, List.of("length less than 4")));

        return incorrectlyFilledFields;
    }

    private static boolean isNullField(Field field, Address address) {
        try {
            field.setAccessible(true);
            if (field.get(address) == null)
                return true;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }

    private static boolean isTooShortString(Field field, Address address) {
        MinLength minLengthAnnotation = field.getAnnotation(MinLength.class);
        int minLengthField = minLengthAnnotation.minLength();

        try {
            field.setAccessible(true);
            Object fieldValue = field.get(address);
            if (fieldValue != null) {
                if (((String) fieldValue).length() < minLengthField) {
                    return true;
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }

}
// END
