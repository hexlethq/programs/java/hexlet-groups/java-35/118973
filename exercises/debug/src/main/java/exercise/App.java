package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int side_1, int side_2, int side_3) {
        boolean isTriangleExist = (side_1 < side_2 + side_3) && (side_1 < side_2 + side_3) && (side_3 < side_2 + side_1);

        if (isTriangleExist) {
            if (side_1 == side_2 && side_2 == side_3) {
                return "Равносторонний";
            } else if (side_1 == side_2 || side_2 == side_3 || side_1 == side_3) {
                return "Равнобедренный";
            } else {
                return "Разносторонний";
            }
        }
        else {
            return "Треугольник не существует";
        }
    }

    public static int getFinalGrade(int exam, int project){
        if (exam > 90 || project > 10) {
            return 100;
        }
        else if (exam > 75 && project > 4) {
            return 90;
        }
        else if (exam > 50 && project > 1) {
            return 75;
        } else {
            return 0;
        }
    }

    // END
}

