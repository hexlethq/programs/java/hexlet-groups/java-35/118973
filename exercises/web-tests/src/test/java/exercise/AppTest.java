package exercise;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import io.javalin.Javalin;
import io.ebean.DB;
import io.ebean.Transaction;

import exercise.domain.User;
import exercise.domain.query.QUser;

class AppTest {

    private static Javalin app;
    private static String baseUrl;
    private static Transaction transaction;

    // BEGIN
    @BeforeAll
    static void beforeAll() {
        app = App.getApp();
        app.start();
        int port = app.port();
        baseUrl = "http://localhost:" + port;
    }

    @AfterAll
    public static void afterAll() {
        app.stop();
    }
    // END

    // Хорошей практикой является запуск тестов с базой данных внутри транзакции.
    // Перед каждым тестом транзакция открывается,
    @BeforeEach
    void beforeEach() {
        transaction = DB.beginTransaction();
    }

    // А после окончания каждого теста транзакция откатывается
    // Таким образом после каждого теста база данных возвращается в исходное состояние,
    // каким оно было перед началом транзакции.
    // Благодаря этому тесты не влияют друг на друга
    @AfterEach
    void afterEach() {
        transaction.rollback();
    }

    @Test
    void testRoot() {
        HttpResponse<String> response = Unirest.get(baseUrl).asString();
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    void testUsers() {
        // Выполняем GET запрос на адрес http://localhost:port/users
        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users")
            .asString();
        // Получаем тело ответа
        String content = response.getBody();

        // Проверяем код ответа
        assertThat(response.getStatus()).isEqualTo(200);
        // Проверяем, что на станице есть определенный текст
        assertThat(content).contains("Wendell Legros");
        assertThat(content).contains("Larry Powlowski");
    }

    @Test
    void testUser() {
        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/5")
            .asString();
        String content = response.getBody();

        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(content).contains("Rolando Larson");
        assertThat(content).contains("galen.hickle@yahoo.com");
    }

    @Test
    void testNewUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/new")
            .asString();

        assertThat(response.getStatus()).isEqualTo(200);
    }

    // BEGIN
    @Test
    void testCreateNewUserValid() {
        HttpResponse<String> response = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "Evgeniy")
                .field("lastName", "Pautov")
                .field("email", "test@gmail.com")
                .field("password", "passwords")
                .asString();

        assertThat(response.getStatus()).isEqualTo(302);

        User actualUser = new QUser()
                .email.equalTo("test@gmail.com")
                .findOne();

        assertThat(actualUser).isNotNull();
        assertThat(actualUser.getFirstName()).isEqualTo("Evgeniy");
        assertThat(actualUser.getLastName()).isEqualTo("Pautov");
        assertThat(actualUser.getPassword()).isEqualTo("passwords");
    }

    @Test
    void testCreateUserNotValidFirstName() {

        HttpResponse<String> response1 = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "")
                .field("lastName", "Evgeniy")
                .field("email", "mail1@gmail.com")
                .field("password", "passwords")
                .asString();

        assertThat(response1.getStatus()).isEqualTo(422);

        User actualUser = new QUser()
                .email.equalTo("mail1@gmail.com")
                .findOne();

        assertThat(actualUser).isNull();

        String body1 = response1.getBody();

        assertThat(body1).contains("Имя не должно быть пустым");
        assertThat(body1).contains("Evgeniy");
        assertThat(body1).contains("mail1@gmail.com");
        assertThat(body1).contains("passwords");
    }

    @Test
    void testCreateUserNotValidLastName() {

        HttpResponse<String> response1 = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "Evgeniy")
                .field("lastName", "")
                .field("email", "mail2@gmail.com")
                .field("password", "passwords")
                .asString();

        assertThat(response1.getStatus()).isEqualTo(422);

        User actualUser = new QUser()
                .email.equalTo("mail2@gmail.com")
                .findOne();

        assertThat(actualUser).isNull();

        String body1 = response1.getBody();

        assertThat(body1).contains("Фамилия не должна быть пустой");
        assertThat(body1).contains("Evgeniy");
        assertThat(body1).contains("mail2@gmail.com");
        assertThat(body1).contains("passwords");
    }

    @Test
    void testCreateUserNotValidEmail() {

        HttpResponse<String> response1 = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "Evgeniy")
                .field("lastName", "Pautov")
                .field("email", "gmail.com")
                .field("password", "passwords")
                .asString();

        assertThat(response1.getStatus()).isEqualTo(422);

        User actualUser = new QUser()
                .email.equalTo("gmail.com")
                .findOne();

        assertThat(actualUser).isNull();

        String body1 = response1.getBody();

        assertThat(body1).contains("Должно быть валидным email");
        assertThat(body1).contains("Evgeniy");
        assertThat(body1).contains("Pautov");
        assertThat(body1).contains("gmail.com");
        assertThat(body1).contains("passwords");
    }

    @Test
    void testCreateUserNotValidPassword() {

        HttpResponse<String> response1 = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "Evgeniy")
                .field("lastName", "Pautov")
                .field("email", "mail3@gmail.com")
                .field("password", "123")
                .asString();

        assertThat(response1.getStatus()).isEqualTo(422);

        User actualUser = new QUser()
                .email.equalTo("mail3@gmail.com")
                .findOne();

        assertThat(actualUser).isNull();

        String body1 = response1.getBody();

        assertThat(body1).contains("Пароль должен содержать не менее 4 символов");
        assertThat(body1).contains("Evgeniy");
        assertThat(body1).contains("Pautov");
        assertThat(body1).contains("mail3@gmail.com");
        assertThat(body1).contains("123");
    }

    // END
}
