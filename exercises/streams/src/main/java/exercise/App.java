package exercise;

import java.util.List;
import java.util.stream.Collectors;

// BEGIN
class App {
    public static int getCountOfFreeEmails(List <String> emails) {
        List<String> freeDomains = List.of("@gmail.com", "@yandex.ru", "@hotmail.com");

        return (int) emails.stream()
                .map(s -> s.substring(s.lastIndexOf("@")))
                .filter(freeDomains::contains)
                .count();
    }
}
// END
