package exercise.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List <Map<String, String>> getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        ObjectMapper objectMapper = new ObjectMapper();
        String fileContent = Files.readString(Paths.get("src","main", "resources", "users.json"));

        List <Map<String, String>> userList = objectMapper.readValue(fileContent, new TypeReference<>() {});

        userList.sort(Comparator.comparingInt(o -> Integer.parseInt(o.get("id"))));

        return userList;
        // END
    }

    private void showUsers(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException {

        // BEGIN
        PrintWriter responseWriter = response.getWriter();
        response.setContentType("text/html;charset=UTF-8");

        List <Map<String, String>> users = getUsers();

        if (users.size() != 0) {
            StringBuilder sb = new StringBuilder();
            responseWriter.write("""
            <!DOCTYPE html>
            <html lang=\"ru\">
                <head>
                    <meta charset=\"UTF-8\">
                    <title>Example application | Users</title>
                </head>
                <body>
                <table>
            """);

            for (Map<String, String> user: users) {
                responseWriter.write(generateUserRecordLine(user));
            }

            responseWriter.write("""
                </table>
                </body>
            </html>
            """);
            responseWriter.write(sb.toString());
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "No users");
        }

        responseWriter.close();
        // END
    }

    private void showUser(HttpServletRequest request,
                         HttpServletResponse response,
                         String id)
                 throws IOException {
        // BEGIN
        List <Map<String, String>> users = getUsers();

        Map<String, String> foundUser = users.stream()
                .filter(u -> u.get("id").equals(id))
                .findAny()
                .orElse(null);

        PrintWriter responseWriter = response.getWriter();
        response.setContentType("text/html;charset=UTF-8");


        if (foundUser != null) {
            StringBuilder stringBuilder = new StringBuilder();
            Map<String, String> filtredUser = users.get(Integer.parseInt(id) - 1);
            stringBuilder.append("""
            <!DOCTYPE html>
            <html lang=\"ru\">
                <head>
                    <meta charset=\"UTF-8\">
                    <title>Example application | Users</title>
                </head>
                <body>
                <table border="1" cellspacing="3" cellpadding="2">
            """);

            stringBuilder.append(generateFullUserRecordLine(filtredUser));

            stringBuilder.append("""
                </table>
                </body>
            </html>
            """);
            responseWriter.write(stringBuilder.toString());
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "Not Found");
        }

        responseWriter.close();
        // END
    }

    private String generateUserRecordLine(Map<String, String> user) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("\n  <tr>");
        stringBuilder.append("\n    <td>" + user.get("id") + "</td>" + "\n    <td>");
        stringBuilder.append("\n      <a href=\"/users/" + user.get("id") + "\">");
        stringBuilder.append(user.get("firstName") + " " + user.get("lastName") + "</a>");
        stringBuilder.append("\n    </td>");
        stringBuilder.append("\n  </tr>");

        return stringBuilder.toString();
    }

    private String generateFullUserRecordLine(Map<String, String> user) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("\n  <tr>");
        stringBuilder.append("\n    <td>id</td>");
        stringBuilder.append("\n    <td>First name</td>");
        stringBuilder.append("\n    <td>Last name</td>");
        stringBuilder.append("\n    <td>e-mail</td>");
        stringBuilder.append("\n  </tr>");
        stringBuilder.append("\n  <tr>");
        stringBuilder.append("\n    <td>" + user.get("id") + "</td>");
        stringBuilder.append("\n    <td>" + user.get("firstName") + "</td>");
        stringBuilder.append("\n    <td>" + user.get("lastName") + "</td>");
        stringBuilder.append("\n    <td>" + user.get("email") + "</td>");
        stringBuilder.append("\n  </tr>");
        stringBuilder.append("\n</table>");

        return stringBuilder.toString();
    }
}
