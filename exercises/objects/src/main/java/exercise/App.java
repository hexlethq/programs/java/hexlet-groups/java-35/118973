package exercise;

import java.time.LocalDate;

class App {
    // BEGIN
    public static String buildList(String[] inputArr){
        if (inputArr.length == 0) {
            return "";
        }

        StringBuilder result = new StringBuilder("<ul>");

        for (String x: inputArr){
            result.append(String.format("\n  <li>%s</li>", x));
        }
        return (result.append("\n</ul>")).toString();
    }

    public static String getUsersByYear(String[][] inputArr, int year){
        int countResultUsers = 0;

        for (int i = 0; i < inputArr.length; i++){
            LocalDate data = LocalDate.parse(inputArr[i][1]);
            if (data.getYear() == year) {
                countResultUsers++;
            }
        }

        if (countResultUsers != 0) {
            String [] arrUser = new String[countResultUsers];
            int indexForArrUser = 0;

            for (int i = 0; i < inputArr.length; i++){
                LocalDate data = LocalDate.parse(inputArr[i][1]);

                if (data.getYear() == year) {
                    arrUser[indexForArrUser] = String.valueOf(inputArr[i][0]);
                    indexForArrUser++;
                }
            }
            return buildList(arrUser);
        }

        return  "";
    }

    public static String getYoungestUser(String[][] users, String date) throws Exception {
        LocalDate beforeDate = LocalDate.parse(date);
        LocalDate youngestUserDate = LocalDate.MIN;
        String youngestUserName = "";

        for (int i = 0; i < users.length; i++) {
            LocalDate currUsrDate = LocalDate.parse(users[i][1]);
            if (currUsrDate.isBefore(beforeDate) && currUsrDate.isAfter(youngestUserDate)) {
                youngestUserDate = currUsrDate;
                youngestUserName = users[i][0];
            }
        }

        return youngestUserName;
    }
    // END
}
