package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.stream.Collectors;
import static exercise.Data.getCompanies;

@WebServlet(name = "/companies")
public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        // BEGIN
        PrintWriter responseWriter = response.getWriter();
        List<String> companies = getCompanies();
        String searchString = request.getParameter("search");

        if (searchString == null || searchString.isEmpty()) {
            companies.forEach(x -> responseWriter.write(x + "\n"));
        } else {
            List <String> filtredCompanies = companies.stream()
                    .filter(x -> x.contains(searchString))
                    .collect(Collectors.toList());

            if (filtredCompanies.size() == 0) {
                responseWriter.write("Companies not found");
            } else {
                filtredCompanies.forEach(x -> responseWriter.write(x + "\n"));
            }

        }

        responseWriter.close();
        // END
    }

}
