package exercise;

class Triangle {
    // BEGIN
    public static void main(String[] args) {
        System.out.println(getSquare(4,5,45));
    }

    public static double getSquare(int firstSide, int secondSide, int angleInDegrees){
        double angleInRadians = angleInDegrees * Math.PI / 180;
        double sinus = Math.sin(angleInRadians);

        return firstSide * secondSide * sinus / 2;
    }
    // END
}
