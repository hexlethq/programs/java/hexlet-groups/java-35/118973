package exercise;

import java.util.Locale;

class Converter {
    // BEGIN
    public static void main(String[] args) {
        int kbCount = 10;
        System.out.println(String.format("%d Kb = %d b",kbCount,convert(10,"b")));
    }

    public static int convert(int dataSize, String measure){
        switch (measure.toLowerCase()){
            case "kb":{
                return dataSize / 1024;
            }
            case "b":{
                return dataSize * 1024;
            }
            default:{
                return 0;
            }
        }
    }
    // END
}
