package exercise;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Stream;

// BEGIN
class App {
    public static String[][] enlargeArrayImage(String[][] image) {
        String [][] enlargedImage = Arrays.stream(image)
                .flatMap(x -> Stream.of(x, x))
                .map(doubleArray)
                .toArray(String[][]::new);

        return enlargedImage;
    }

    static Function<String[], String[]> doubleArray = strings -> Arrays.stream(strings)
            .flatMap(x -> Stream.of(x, x))
            .toArray(String[]::new);
}
// END
