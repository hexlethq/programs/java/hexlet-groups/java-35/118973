package exercise;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

// BEGIN
class SorterTest {
    @Test
    void takeOldestMans_Test_With_Data() {
        List<Map<String, String>> users1 = List.of(
                Map.of("name", "Vladimir Nikolaev", "birthday", "1990-12-27", "gender", "male"),
                Map.of("name", "Andrey Petrov", "birthday", "1989-11-23", "gender", "male"),
                Map.of("name", "Anna Sidorova", "birthday", "1996-09-09", "gender", "female"),
                Map.of("name", "John Smith", "birthday", "1989-03-11", "gender", "male"),
                Map.of("name", "Vanessa Vulf", "birthday", "1985-11-16", "gender", "female"),
                Map.of("name", "Alice Lucas", "birthday", "1986-01-01", "gender", "female"),
                Map.of("name", "Elsa Oscar", "birthday", "1970-03-10", "gender", "female")
        );

        List<String> takeMans = List.of("John Smith", "Andrey Petrov", "Vladimir Nikolaev");

        List<String> actual1 = Sorter.takeOldestMans(users1);
        assertThat(actual1).isEqualTo(takeMans);
    }

    @Test
    void takeOldestMans_Returns_Empty() {
        List<Map<String, String>> users1 = new ArrayList<>();
        List<String> actual1 = Sorter.takeOldestMans(users1);
        assertThat(actual1).isEmpty();
    }
}
// END


