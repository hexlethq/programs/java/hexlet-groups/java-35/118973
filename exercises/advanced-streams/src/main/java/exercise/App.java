package exercise;

import java.util.stream.Collectors;
import java.util.Arrays;

// BEGIN
class App {
    public static final String PREFIX = "X_FORWARDED_";
    public static final String ENVIRONMENT = "environment";
    public static final String DELIMITER = ",";

    public static String getForwardedVariables(String configFileData) {

        return Arrays.stream(configFileData.split("\n"))
                .filter(x -> x.startsWith(ENVIRONMENT) && x.contains(PREFIX))
                .map(x -> x.substring(x.indexOf('\"') + 1, x.lastIndexOf("\"")))
                .flatMap(x -> Arrays.stream(x.split(DELIMITER)))
                .filter(x -> x.startsWith(PREFIX))
                .map(x -> x.substring(PREFIX.length()))
                .collect(Collectors.joining(","));
    }
}
//END
