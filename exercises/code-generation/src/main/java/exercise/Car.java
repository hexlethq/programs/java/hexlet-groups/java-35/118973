package exercise;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Value;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

@AllArgsConstructor
@Value
@Getter
class Car {
    int id;
    String brand;
    String model;
    String color;
    User owner;

    // BEGIN
    public String serialize() {
        ObjectMapper objectMapper = new ObjectMapper();
        String result = "";
        try {
            result = objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static Car unserialize(String carInJson) {
        ObjectMapper objectMapper = new ObjectMapper();
        Car result = null;
        try {
            result = objectMapper.readValue(carInJson, Car.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
    // END
}
