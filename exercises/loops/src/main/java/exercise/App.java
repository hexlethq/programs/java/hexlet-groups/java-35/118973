package exercise;

import java.util.Locale;

class App {
    // BEGIN
    public static String getAbbreviation(String str){
        if (str.isEmpty()){
            return "";
        }
        String abbreviation = String.valueOf(str.charAt(0));

        for (int i = 1; i < str.length(); i++) {
            if (str.charAt(i) != ' ' && str.charAt(i - 1) == ' ') {
                abbreviation += str.charAt(i);
            }
        }
        return (abbreviation.toUpperCase()).trim();
    }
    // END
}
