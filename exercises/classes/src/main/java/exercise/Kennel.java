package exercise;

import java.util.Arrays;

// BEGIN
class Kennel {
    private static String[][] puppies;

    public static void addPuppy(String[] newPuppy) {
        int countPuppies = puppies != null ? puppies.length : 0;
        String [][] newArrPuppies = new String[countPuppies + 1][];

        for (int i = 0 ;i < countPuppies; i++) {
            newArrPuppies[i] = puppies[i].clone();
        }
        newArrPuppies[countPuppies] = new String[2];
        puppies = newArrPuppies;

        puppies[countPuppies][0] = newPuppy[0];
        puppies[countPuppies][1] = newPuppy[1];
    }

    public static void addSomePuppies(String[][] newPuppies) {
        for (String [] nextPuppy: newPuppies) {
            Kennel.addPuppy(nextPuppy);
        }
    }

    public static int getPuppyCount() {
        return puppies.length;
    }

    public static boolean isContainPuppy(String namePuppy) {
        for (String[] x: puppies) {
            if (x[0].equals(namePuppy)){
                return true;
            }
        }
        return  false;
    }

    public static String[][] getAllPuppies() {
        String [][] result = new String[puppies.length][];
        for (int i = 0 ; i < puppies.length; i++) {
            result[i] = puppies[i].clone();
        }
        return result;
    }

    public static String[] getNamesByBreed(String breed) {
        int countFoundedPuppy = 0;

        for (String[] x: puppies) {
            if (x[1].equals(breed)){
                countFoundedPuppy++;
            }
        }

        String [] result = new String[countFoundedPuppy];
        int counter = 0;

        for (String[] x: puppies) {
            if (x[1].equals(breed)){
                result[counter] = x[0];
                counter++;
            }
        }
        return  result;
    }

    public static void resetKennel() {
        puppies = new String[0][];
    }

    public static boolean removePuppy(String namePuppy) {
        int countPuppies = puppies.length;

        for (int i = 0 ; i < countPuppies; i++) {
            if (puppies[i][0].equals(namePuppy)) {
                String [][] newArrPuppies = new String[countPuppies - 1][2];
                int indexToWrite = 0;
                for (int j = 0 ;j < countPuppies; j++) {
                    if (i != j) {
                        newArrPuppies[indexToWrite] = puppies[j];
                        indexToWrite++;
                    }
                }
                puppies = newArrPuppies;

                return true;
            }
        }

        return  false;
    }
}
// END