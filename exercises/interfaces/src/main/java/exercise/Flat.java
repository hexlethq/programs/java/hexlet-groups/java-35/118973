package exercise;

// BEGIN
class Flat implements Home {

    private double livingArea;
    private double balconyArea;
    private int floor;

    public Flat(double livingArea, double balconyArea, int floor) {
        this.livingArea = livingArea;
        this.balconyArea = balconyArea;
        this.floor = floor;
    }

    @Override
    public String toString() {
        return String.format("Квартира площадью %s метров на %d этаже", getArea(), floor);
    }

    @Override
    public double getArea() {
        return livingArea + balconyArea;
    }

    @Override
    public int compareTo(Home homeToCompare) {
        double thisArea = getArea();
        double thatArea = homeToCompare.getArea();

        return (thisArea > thatArea) ? 1 : ((thisArea == thatArea) ? 0 : -1);
    }
}
// END
