package exercise;

import java.util.Comparator;
import java.util.List;

// BEGIN
class App {

    public static List<String> buildApartmentList(List<Home> listOfRealEstate, int numOfFirstElements) {
        return listOfRealEstate.stream()
                .sorted(Comparator.comparingDouble(Home::getArea))
                .limit(numOfFirstElements)
                .map(home -> home.toString())
                .toList();
    }

}
// END
