package exercise;

// BEGIN
class Cottage implements Home {

    private double totalArea;
    private int floorCount;

    public Cottage(double totalArea, int floorCount) {
        this.totalArea = totalArea;
        this.floorCount = floorCount;
    }

    @Override
    public double getArea() {
        return totalArea;
    }

    @Override
    public int compareTo(Home homeToCompare) {
        double thisArea = getArea();
        double thatArea = homeToCompare.getArea();

        return (thisArea > thatArea) ? 1 : ((thisArea == thatArea) ? 0 : -1);
    }

    @Override
    public String toString() {
        return String.format("%d этажный коттедж площадью %.1f метров", floorCount, totalArea);
    }

}
// END
