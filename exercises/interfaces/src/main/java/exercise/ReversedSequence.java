package exercise;

// BEGIN
class ReversedSequence implements CharSequence {

    private String sentence;

    public ReversedSequence(String sentence) {
        this.sentence = new StringBuilder(sentence)
                .reverse()
                .toString();
    }

    @Override
    public String toString() {
        return sentence.toString();
    }

    @Override
    public int length() {
        return sentence.length();
    }

    @Override
    public char charAt(int i) {
        return sentence.charAt(i - 1);
    }

    @Override
    public CharSequence subSequence(int i, int i1) {
        return sentence.substring(i, i1);
    }
}
// END
