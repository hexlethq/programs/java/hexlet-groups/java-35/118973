package exercise;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
class PairedTag extends Tag {

    private final String body;
    private final List<Tag> childList;

    public PairedTag(String tagName, Map<String, String> attributesAndValues, String body, List<Tag> childList) {
        this.tagName = tagName;
        this.attributesAndValues = new LinkedHashMap<>(attributesAndValues);
        this.body = body;
        this.childList = new LinkedList<>(childList);
    }

    @Override
    public String toString() {
        return new StringBuilder().append("<")
                .append(tagName)
                .append(tagAttributesToString())
                .append(">")
                .append(body)
                .append(generateChildrenTags())
                .append("</")
                .append(tagName)
                .append(">")
                .toString();

    }

    private String generateChildrenTags() {
        return childList.stream()
                .map(Tag::toString)
                .collect(Collectors.joining());
    }

}
// END
