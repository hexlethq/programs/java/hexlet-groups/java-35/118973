package exercise;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

// BEGIN
class SingleTag extends Tag {

    public SingleTag(String tagName, Map<String, String> attributesAndValues) {
        this.tagName = tagName;
        this.attributesAndValues = new LinkedHashMap<>(attributesAndValues);
    }

    @Override
    public String toString() {
        return new StringBuilder().append("<")
                .append(tagName)
                .append(tagAttributesToString())
                .append(">")
                .toString();
    }

}
// END
