package exercise;

import java.util.Map;
import java.util.stream.Collectors;


// BEGIN
abstract class Tag {
    String tagName;
    Map <String, String> attributesAndValues;

    protected String createStringFromEntry(Map.Entry <String, String> entry) {
        return new StringBuilder().append(" ")
                .append(entry.getKey())
                .append("=\"")
                .append(entry.getValue())
                .append("\"")
                .toString();
    }

    protected String tagAttributesToString() {
        return attributesAndValues.entrySet().stream()
                .map(this::createStringFromEntry)
                .collect(Collectors.joining());
    }
}
// END
