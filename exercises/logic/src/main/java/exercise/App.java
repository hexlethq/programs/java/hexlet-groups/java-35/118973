package exercise;

class App {
    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable = (number % 2 == 1) && (number > 1001);
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        System.out.println(number % 2 == 0 ? "yes" : "no");
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        int quarterHour = minutes / 15;
        switch (quarterHour){
            case 0: {
                System.out.println("First");
                break;
            }
            case 1: {
                System.out.println("Second");
                break;
            }
            case 2: {
                System.out.println("Third");
                break;
            }
            case 3: {
                System.out.println("Fourth");
                break;
            }
        }
        // END
    }
}

