package exercise;

// BEGIN
public class NegativeRadiusException extends Exception {
    private String message;

    public NegativeRadiusException(String s) {
        this.message = s;
    }
}
// END
