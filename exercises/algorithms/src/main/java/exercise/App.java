package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] sort(int[] arr) {
        int[] resultArr = arr.clone();
        int sortedElements = 0;
        boolean isSorted = false;

        while (!isSorted){
            isSorted = true;
            for (int j = 0; j < resultArr.length - 1 - sortedElements; j++){
                if (resultArr[j] > resultArr[j+1]){
                    int temp = resultArr[j+1];
                    resultArr[j+1] = resultArr[j];
                    resultArr[j] = temp;
                    isSorted = false;
                }
            }
            sortedElements++;
        }
        return  resultArr;
    }

    // Самостоятельная работа. Сортировка выбором
    public static int[] insertionSort(int[] arr) {
        int[] resultArr = arr.clone();

        for (int i = 0; i < resultArr.length - 1; i++){
            for (int j = i + 1; j < resultArr.length; j++){
                if (resultArr[i] > resultArr[j]){
                    int temp = resultArr[j];
                    resultArr[j] = resultArr[i];
                    resultArr[i] = temp;
                }
            }
        }

        return  resultArr;
    }
    // END
}
