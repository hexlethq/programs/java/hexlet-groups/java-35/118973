package exercise;

class Card {
    public static void main(String[] args) {
        printHiddenCard("5469550034338134",4);
    }
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String hiddenPart = "*".repeat(starsCount);
        String visiblePart = cardNumber.substring(cardNumber.length() - 4);

        System.out.println(hiddenPart + visiblePart);
        // END
    }
}
