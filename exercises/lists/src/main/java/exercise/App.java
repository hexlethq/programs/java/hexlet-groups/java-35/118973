package exercise;

import java.util.ArrayList;
import java.util.List;

// BEGIN
class App {
    public static boolean scrabble(String chars, String word) {
        List<Character> list = new ArrayList<>();
        String normalizedWord = word.toLowerCase();

        for (char c: chars.toCharArray()) {
            list.add(c);
        }

        for (Character ch: normalizedWord.toCharArray()) {
            if (list.contains(ch)) {
                list.remove(ch);
            } else {
                return false;
            }
        }

        return true;
    }
}

//END
