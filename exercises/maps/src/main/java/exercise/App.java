package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
class App {
    public static Map <String, Integer> getWordCount(String sentence) {
        Map <String, Integer> wordCount = new HashMap<>();
        if (!sentence.isEmpty()) {
            String[] words = sentence.split(" ");
            for (String word: words) {
                Integer countWord = wordCount.get(word);
                if (countWord == null) {
                    countWord = 0;
                }
                wordCount.put(word, countWord + 1);
            }
        }
        return wordCount;
    }

    public static String toString(Map <String, Integer> wordCount) {
        StringBuilder stringBuilder = new StringBuilder();

        if (!wordCount.isEmpty()) {
            stringBuilder.append(" {");
            for (String key: wordCount.keySet()) {
                stringBuilder.append("\n  ")
                        .append(key)
                        .append(": ").append(wordCount.get(key));
            }
            stringBuilder.append("\n}");
        } else {
            stringBuilder.append("{}");
        }

        return stringBuilder.toString();
    }

}
//END
