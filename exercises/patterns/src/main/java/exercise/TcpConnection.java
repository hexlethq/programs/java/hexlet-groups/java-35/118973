package exercise;
import exercise.connections.Connection;
import exercise.connections.Disconnected;

// BEGIN
public class TcpConnection {

    private String ipAddress;
    private int port;
    private String data;
    private Connection connectionState;

    public TcpConnection(String ipAddress, int port) {
        this.ipAddress = ipAddress;
        this.port = port;
        this.connectionState = new Disconnected(this);
    }

    public void connect() {
        connectionState.connect();
    }

    public void disconnect() {
        connectionState.disconnect();
    }

    public String getCurrentState() {
        return connectionState.getCurrentState();
    }

    public void write(String data) {
        this.connectionState.write(data);
    }

    public void setConnectionState(Connection connectionState) {
        this.connectionState = connectionState;
    }

    public void setData(String newData) {
        this.data += newData;
    }
}
// END
