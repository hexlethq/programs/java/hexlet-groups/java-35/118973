package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public final class Disconnected implements Connection {

    private TcpConnection tcpConnection;

    public Disconnected(TcpConnection tcpConnection) {

        this.tcpConnection = tcpConnection;
    }

    @Override
    public void connect() {
        this.tcpConnection.setConnectionState(new Connected(this.tcpConnection));
    }

    @Override
    public void disconnect() {
        System.out.println("Error! Connection already disconnected");
    }

    @Override
    public void write(String data) {
        System.out.println("Error! Connection disconnected");
    }

    @Override
    public String getCurrentState() {
        return "disconnected";
    }
}
// END
