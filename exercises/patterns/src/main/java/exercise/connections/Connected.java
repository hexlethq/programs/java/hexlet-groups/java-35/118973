package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public final class Connected implements Connection {

    private TcpConnection tcpConnection;

    public Connected(TcpConnection tcpConnection) {
        this.tcpConnection = tcpConnection;
    }

    @Override
    public void disconnect() {
        this.tcpConnection.setConnectionState(new Disconnected(this.tcpConnection));
    }

    @Override
    public void write(String data) {
        this.tcpConnection.setData(data);
    }

    @Override
    public void connect() {
        System.out.println("Error! Connection already established");
    }

    @Override
    public String getCurrentState() {
        return "connected";
    }
}
// END
