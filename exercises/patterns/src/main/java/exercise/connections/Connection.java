package exercise.connections;

public abstract interface Connection {
    // BEGIN
    void connect();

    void disconnect();

    void write(String data);

    String getCurrentState();
    // END
}
