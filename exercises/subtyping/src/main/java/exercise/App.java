package exercise;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

// BEGIN
class App {
    public static void swapKeyValue(KeyValueStorage kV) {
        Map <String, String> tmpKV = kV.toMap();

        for (Map.Entry<String, String> pair: tmpKV.entrySet()) {
            String newValue = pair.getKey();
            String newKey = pair.getValue();

            kV.unset(newValue);
            kV.set(newKey, newValue);
        }
    }
}
// END
