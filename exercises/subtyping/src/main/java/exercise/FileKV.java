package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
class FileKV implements KeyValueStorage {

    private String filePath;
    private Map <String, String> dictionary;

    public FileKV(String filePath, Map<String, String> baseDictionary) {
        this.filePath = filePath;
        dictionary = new HashMap<>(baseDictionary);
        updateFileContent();
    }

    @Override
    public void set(String key, String value) {
        dictionary.put(key, value);
        updateFileContent();
    }

    @Override
    public void unset(String key) {
        dictionary.remove(key);
        updateFileContent();
    }

    @Override
    public String get(String key, String defaultValue) {
        return dictionary.getOrDefault(key, defaultValue);
    }

    @Override
    public Map<String, String> toMap() {
        return new HashMap<>(dictionary);
    }

    private void updateFileContent() {
        Utils.writeFile(filePath, Utils.serialize(dictionary));
    }
}
// END
