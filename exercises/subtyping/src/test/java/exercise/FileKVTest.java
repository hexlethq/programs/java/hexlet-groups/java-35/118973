package exercise;

import java.io.IOException;
import java.util.HashMap;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;


class FileKVTest {

    private static Path filepath = Paths.get("src/test/resources/file").toAbsolutePath().normalize();
    private static String path = filepath.toString();
    private KeyValueStorage storage;

    @BeforeEach
    public void beforeEach() throws Exception {
        storage = new FileKV(path,
                Map.of("key1", "value1",
                        "key2", "value2",
                        "key3", "value3"));
    }

    // BEGIN
    @Test
    void testGet() {
        String actual = storage.get("key1", "default");
        Assertions.assertEquals(actual, "value1");

        String actual2 = storage.get("key3", "default");
        Assertions.assertEquals(actual2, "value3");

        String actual3 = storage.get("key4", "default");
        Assertions.assertEquals(actual3, "default");
    }

    @Test
    void testFileContent() {
        try {
            String actual = Files.readString(filepath);
            String expected = "{\"key1\":\"value1\",\"key2\":\"value2\",\"key3\":\"value3\"}";
            Assertions.assertEquals(expected, actual);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testSet() {
        storage.set("key2", "value4");
        storage.set("key4", "value5");

        Map<String, String> expected = Map.of(
                "key1", "value1",
                "key2", "value4",
                "key3", "value3",
                "key4", "value5");

        Map<String, String> actual = storage.toMap();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testUnset() {
        storage.unset("key5");
        storage.unset("key3");

        Map<String, String> expected = Map.of(
                "key1", "value1",
                "key2", "value2");

        Map<String, String> actual = storage.toMap();
        Assertions.assertEquals(expected, actual);
    }
    // END
}
