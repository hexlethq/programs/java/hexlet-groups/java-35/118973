package exercise;

import java.util.Arrays;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y){
        return new int[] {x, y};
    }

    public static int getX(int[] inputPoint){
        return inputPoint[0];
    }

    public static int getY(int[] coordinates){
        return coordinates[1];
    }

    public static String pointToString(int[] coordinates){
        return String.format("(%d, %d)", getX(coordinates), getY(coordinates));
    }

    public static int getQuadrant(int[] coordinates){
        int x = getX(coordinates);
        int y = getY(coordinates);

        if (x == 0 || y == 0){
            return 0;
        }
        else if (x > 0){
            return y > 0 ? 1 : 4;
        }
        else {
            return y > 0 ? 2 : 3;
        }
    }

    public static int[] getSymmetricalPointByX (int[] coordinates){
        int [] symmetricalPoint = coordinates.clone();

        if (symmetricalPoint[0] == 0 ) {
            return symmetricalPoint;
        }
        else {
            return (new int[] {getX(coordinates), -getY(coordinates)});
        }
    }

    public static double calculateDistance(int[] dot1, int[] dot2){
        double part1 = Math.pow(Point.getX(dot2) - Point.getX(dot1), 2);
        double part2 = Math.pow(Point.getY(dot2) - Point.getY(dot1), 2);

        return Math.sqrt(part1 + part2);
    }
    // END
}
