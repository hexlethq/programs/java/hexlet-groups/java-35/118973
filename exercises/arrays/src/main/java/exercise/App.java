package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] reverse(int[] arr){
        int[] newArr = new int[arr.length];

        for (int i = 0; i < newArr.length; i++){
            newArr[i] = arr[arr.length - 1 - i];
        }

        return newArr;
    }

    public static int mult(int[] arr){
        int result = 1;

        for (int x: arr){
            result *= x;
        }

        return  result;
    }

    public static int[] flattenMatrix(int[][] Matrix) {
        if (Matrix.length != 0) {
            int[] result = new int[Matrix.length * Matrix[0].length];
            int indexToWrite = 0;

            for (int i = 0; i < Matrix.length; i++) {
                for (int j = 0; j < Matrix[0].length; j++) {
                    result[indexToWrite] = Matrix[i][j];
                    indexToWrite++;
                }
            }

            return result;
        } else {
            return (new int[0]);
        }
    }
    // END
}
