<%@ page import = "java.util.List" %>
<%@ page import = "java.util.Map" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- BEGIN -->
<html>
    <head>
        <title>Users</title>
    </head>
    <body>
        <%
            List<Map<String, String>> users = (List<Map<String, String>>) request.getAttribute("users");
        %>
        <table>
            <%
                for (int i = 0; i < users.size(); i++) {
                String userId = users.get(i).get("id");
                %>
            <tr>
                <td><a href=<%= "/users/show?id=" + userId %>><%= userId %></a></td>
                <td><%= users.get(i).get("firstName") %></td>
                <td><%= users.get(i).get("lastName") %></td>
                <td><%= users.get(i).get("email") %></td>
            </tr>
            <%}%>
        </table>
    </body>
</html>>
<!-- END -->
