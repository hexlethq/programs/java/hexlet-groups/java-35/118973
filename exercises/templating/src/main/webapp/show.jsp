<%@ page import = "java.util.Map" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- BEGIN -->
<html>
    <head>
        <title>User</title>
    </head>
    <body>
        <%
            Map<String, String> user = (Map<String, String>) request.getAttribute("user");
        %>
        <table border="1" cellspacing="3" cellpadding="2">
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Email</td>
            </tr>
            <tr>
                <td><%= user.get("id") %></td>
                <td><%= user.get("firstName") + " " + user.get("lastName") %></td>
                <td><%= user.get("email") %></td>
            </tr>
        </table>
        <p><a href=<%= "/users/delete?id=" + user.get("id") %>>Delete user</a></p>
    </body>
</html>
<!-- END -->