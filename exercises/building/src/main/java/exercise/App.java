// BEGIN
package exercise;

import com.google.gson.Gson;

class App {
    public static void main(String[] args) {
        System.out.print("Hello, World!");
    }

    public static String toJson(String[] strings) {
        Gson gson = new Gson();
        return gson.toJson(strings);
    }
}
// END
